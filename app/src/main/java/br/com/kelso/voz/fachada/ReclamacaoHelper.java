package br.com.kelso.voz.fachada;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import br.com.kelso.voz.R;
import br.com.kelso.voz.ReclamacaoActivity;
import br.com.kelso.voz.dao.ReclamacaoDao;
import br.com.kelso.voz.exception.UserVazioException;
import br.com.kelso.voz.modelo.Reclamacao;

/**
 * Created by kelso on 19/10/2017.
 */

public class ReclamacaoHelper {
    public EditText campoEndereco;
    public EditText campoDescricao;
    private ReclamacaoDao dao;
    private Reclamacao reclamacao;
    private ArrayList<EditText> campos = new ArrayList<EditText>();
    private ReclamacaoActivity activity;
    private ImageView campoFoto;
    public ReclamacaoHelper(ReclamacaoActivity activity) {
        campos.add(this.campoEndereco = (EditText) activity.findViewById(R.id.reclamacao_endereco));
        campos.add(this.campoDescricao = (EditText) activity.findViewById(R.id.reclamacao_descricao));
        campoFoto = (ImageView) activity.findViewById(R.id.reclamacao_foto);
        this.dao = new ReclamacaoDao(activity);
    }


    private boolean camposVazio(){
        for (EditText campo: campos) {
            String str = campo.getText().toString();
            if (TextUtils.isEmpty(str)) {
                return true;
            }
        }
        return false;
    }

    public Reclamacao pegaCadastro() throws UserVazioException{

        if(!camposVazio()){
            reclamacao = new Reclamacao();
            reclamacao.setDescricao(campoDescricao.getText().toString());
            reclamacao.setEndereco(campoEndereco.getText().toString());
            reclamacao.setCaminhoFoto((String) campoFoto.getTag());
            return reclamacao;
        }else if (camposVazio()){
            throw new UserVazioException();
        }
        return null;
    }


    public void carregaImagem(String caminhoFoto) {
        if(caminhoFoto != null) {
            //Abrindo a imagen Aqui.

            Bitmap bitmap = BitmapFactory.decodeFile(caminhoFoto);
            Bitmap bitmapReduzido = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            campoFoto.setImageBitmap(bitmapReduzido);
            campoFoto.setScaleType(ImageView.ScaleType.FIT_XY);
            campoFoto.setTag(caminhoFoto);
        }
    }
}
