package br.com.kelso.voz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import br.com.kelso.voz.exception.UserEmailErradoException;
import br.com.kelso.voz.exception.UserSenhaErradaException;
import br.com.kelso.voz.exception.UserVazioException;
import br.com.kelso.voz.fachada.LoginHelper;

public class LoginActivity extends AppCompatActivity  {

    private LoginHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        helper = new LoginHelper(this);


        Button botaoEntrar = (Button) findViewById(R.id.login_entrar);
        Button botaoCadastrar = (Button) findViewById(R.id.login_cadastrar);

        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vaiCadastro = new Intent(LoginActivity.this, CadastrarActivity.class);
                startActivity(vaiCadastro);
                helper.campoEmail.setText("");
                helper.campoSenha.setText("");

            }
        });


        botaoEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(helper.logar()){
                        Intent vaiReclamacao = new Intent(LoginActivity.this, ReclamacaoActivity.class);
                        startActivity(vaiReclamacao);
                        helper.campoEmail.setText("");
                        helper.campoSenha.setText("");
                    }
                } catch (UserEmailErradoException e) {
                    e.printStackTrace();
                    helper.campoEmail.setError("Usuario não Cadastrado");
                } catch (UserSenhaErradaException e) {
                    e.printStackTrace();
                    helper.campoSenha.setError("Senha Incorreta");
                    helper.campoSenha.setText("");
                } catch (UserVazioException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Por Favor Preencha os Campos!!", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }
}
