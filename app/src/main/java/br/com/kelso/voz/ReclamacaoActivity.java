package br.com.kelso.voz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;

import br.com.kelso.voz.dao.ReclamacaoDao;
import br.com.kelso.voz.exception.UserVazioException;
import br.com.kelso.voz.fachada.ReclamacaoHelper;
import br.com.kelso.voz.modelo.Reclamacao;

public class ReclamacaoActivity extends AppCompatActivity  {

    public static final int CODIGO_CAMERA = 123;
    private String[] categorias = {"Categoria", "Iluminação", "Esgoto", "Vias Publicas", "Infraestrutura"};
    private Spinner spinner;
    private String caminhoFoto;
    private ReclamacaoHelper helper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reclamacao);
        helper = new ReclamacaoHelper(this);

        spinner = (Spinner) findViewById(R.id.reclamacao_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, categorias);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);



        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Button botaoFoto = (Button) findViewById(R.id.reclamacao_botao_foto);

        botaoFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                caminhoFoto = getExternalFilesDir(null)+ "/" + System.currentTimeMillis() +".jpg";
                File arquivoFoto = new File(caminhoFoto);
                intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(arquivoFoto));
                startActivityForResult(intentCamera, CODIGO_CAMERA);


            }
        });


        Button botaoEnviar = (Button) findViewById(R.id.reclamacao_enviar);
        botaoEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome = (String) spinner.getSelectedItem();
                ImageView foto = (ImageView) findViewById(R.id.reclamacao_foto);
                try{
                    if(helper.pegaCadastro() != null){
                        Reclamacao reclamacao = helper.pegaCadastro();
                        reclamacao.setCategoria(nome);
                        ReclamacaoDao dao = new ReclamacaoDao(ReclamacaoActivity.this);
                        dao.insere(reclamacao);
                        Toast.makeText(ReclamacaoActivity.this, "Reclamação Cadastrada com Sucesso!!", Toast.LENGTH_SHORT).show();
                        Drawable drawable= getResources().getDrawable(R.drawable.fundo_avatar);
                        foto.setImageDrawable(drawable);
                        helper.campoEndereco.setText("");
                        helper.campoDescricao.setText("");
                    }
                } catch (UserVazioException e) {
                    e.printStackTrace();
                    Toast.makeText(ReclamacaoActivity.this, "Por Favor Preencha os Campos!!", Toast.LENGTH_SHORT).show();
                }
            }
        });



        Button botaoVermais = (Button) findViewById(R.id.reclamacao_ver_mais);
        botaoVermais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vaiLista = new Intent(ReclamacaoActivity.this, ListaActivity.class);
                startActivity(vaiLista);
            }
        });



    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            if (requestCode == CODIGO_CAMERA) {
                helper.carregaImagem(caminhoFoto);

            }

        }

    }

//    public void showElemento(View v){
//        String nome = (String) spinner.getSelectedItem();
//        Toast.makeText(this, "nome: "+ nome,Toast.LENGTH_SHORT).show();
//    }



}
