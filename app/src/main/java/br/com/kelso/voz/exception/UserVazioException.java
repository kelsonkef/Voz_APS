package br.com.kelso.voz.exception;

/**
 * Created by kelso on 07/10/2017.
 */

public class UserVazioException extends Exception {

    public UserVazioException() {
        super("Por favor preenchar os Campos Vazios");
    }

}
