package br.com.kelso.voz.exception;

/**
 * Created by kelso on 07/10/2017.
 */

public class UserSenhaErradaException extends Exception {

    public UserSenhaErradaException(){
        super("Campos de Senha não se coincidem");
    }

}
