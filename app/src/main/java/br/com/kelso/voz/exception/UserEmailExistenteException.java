package br.com.kelso.voz.exception;

/**
 * Created by kelso on 09/10/2017.
 */

public class UserEmailExistenteException extends Exception {

    public UserEmailExistenteException() {
        super("Campos de Email não se coincidem");
    }
}
