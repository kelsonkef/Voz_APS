package br.com.kelso.voz.fachada;

import android.text.TextUtils;
import android.widget.EditText;

import java.util.ArrayList;

import br.com.kelso.voz.LoginActivity;
import br.com.kelso.voz.R;
import br.com.kelso.voz.dao.UserDao;
import br.com.kelso.voz.exception.UserEmailErradoException;
import br.com.kelso.voz.exception.UserSenhaErradaException;
import br.com.kelso.voz.exception.UserVazioException;
import br.com.kelso.voz.modelo.User;

/**
 * Created by kelso on 08/10/2017.
 */

public class LoginHelper {

    public EditText campoEmail;
    public EditText campoSenha;
    private User usuario;
    private UserDao dao;

    private ArrayList<EditText> campos = new ArrayList<EditText>();

    public LoginHelper(LoginActivity activity){
        campos.add(campoEmail = (EditText) activity.findViewById(R.id.login_emai));
        campos.add(campoSenha = (EditText) activity.findViewById(R.id.login_senha));
        dao = new UserDao(activity);

    }
    private boolean camposVazio(){
        for (EditText campo: campos) {
            String str = campo.getText().toString();
            if (TextUtils.isEmpty(str)) {
                return true;
            }
        }
        return false;
    }


    public boolean logar() throws UserEmailErradoException, UserSenhaErradaException, UserVazioException{
        String senha =campoSenha.getText().toString();
        String email = campoEmail.getText().toString();
        if(!camposVazio()){
            if(dao.existeEmail(email)){
                usuario = dao.retornaUserEmail(email);
                if(usuario.getSenha().equals(senha)){
                    return true;
                }else{
                    throw  new UserSenhaErradaException();
                }

            }
            throw  new UserEmailErradoException();
        }
        throw new UserVazioException();
    }




}
