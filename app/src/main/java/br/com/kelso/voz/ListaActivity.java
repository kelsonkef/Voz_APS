package br.com.kelso.voz;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.kelso.voz.adpter.ReclamacaoAdapter;
import br.com.kelso.voz.dao.ReclamacaoDao;
import br.com.kelso.voz.dao.UserDao;
import br.com.kelso.voz.modelo.Reclamacao;
import br.com.kelso.voz.modelo.User;

public class ListaActivity extends AppCompatActivity {

    private ListView listaReclamacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);



        listaReclamacao = (ListView) findViewById(R.id.lista_lista);
        registerForContextMenu(listaReclamacao);

    }

    private void carregaLista() {
        ReclamacaoDao dao = new ReclamacaoDao(this);
        List<Reclamacao> reclamacaos = dao.buscaReclamacoes();
        dao.close();

        listaReclamacao = (ListView) findViewById(R.id.lista_lista);
        ReclamacaoAdapter adapter = new ReclamacaoAdapter(this, reclamacaos);
       // ArrayAdapter<Reclamacao> adapter = new ArrayAdapter<Reclamacao>(this, android.R.layout.simple_list_item_1, reclamacaos);
        listaReclamacao.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        final Reclamacao reclamacao = (Reclamacao) listaReclamacao.getItemAtPosition(info.position);

        MenuItem itemMapa = menu.add("Mostra no Mapa");
        Intent intentMapa = new Intent(Intent.ACTION_VIEW);
        intentMapa.setData(Uri.parse("geo:0,0?q=" + reclamacao.getEndereco()));
        itemMapa.setIntent(intentMapa);

        MenuItem deletar = menu.add("Deletar");
        deletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {


                ReclamacaoDao dao = new ReclamacaoDao(ListaActivity.this);
                Toast.makeText(ListaActivity.this, "Reclamação "+ reclamacao.getDescricao()+" Deletado",Toast.LENGTH_SHORT).show();
                dao.deleta(reclamacao);
                dao.close();
                carregaLista();
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        carregaLista();

    }
}