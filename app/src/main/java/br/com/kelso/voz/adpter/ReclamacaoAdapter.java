package br.com.kelso.voz.adpter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.kelso.voz.R;
import br.com.kelso.voz.modelo.Reclamacao;

/**
 * Created by kelso on 19/10/2017.
 */

public class ReclamacaoAdapter extends BaseAdapter {


    private final List<Reclamacao> reclamacaos;
    private final Context context;

    public ReclamacaoAdapter(Context context, List<Reclamacao> reclamacaos) {
        this.context = context;
        this.reclamacaos = reclamacaos;
    }

    @Override
    public int getCount() {
        return reclamacaos.size();
    }

    @Override
    public Object getItem(int position) {
        return reclamacaos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return reclamacaos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Reclamacao reclamacao = reclamacaos.get(position);

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_item, null);

        TextView campoNome = (TextView) view.findViewById(R.id.item_descricao);
        campoNome.setText(reclamacao.getDescricao());

        TextView campoTelefone = (TextView) view.findViewById(R.id.item_categoria);
        campoTelefone.setText(reclamacao.getCategoria());

        ImageView campoFoto = (ImageView) view.findViewById(R.id.item_foto);
        String caminhoFoto = reclamacao.getCaminhoFoto();
        if(caminhoFoto != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(caminhoFoto);
            Bitmap bitmapReduzido = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
            campoFoto.setImageBitmap(bitmapReduzido);
            campoFoto.setScaleType(ImageView.ScaleType.FIT_XY);
        }

        return view;
    }
}


