package br.com.kelso.voz.exception;

/**
 * Created by kelso on 07/10/2017.
 */

public class UserEmailErradoException extends Exception {

    public UserEmailErradoException() {
        super("Campos de Email não se coincidem");
    }
}
