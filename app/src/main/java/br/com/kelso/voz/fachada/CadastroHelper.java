package br.com.kelso.voz.fachada;

import android.text.TextUtils;
import android.widget.EditText;

import java.util.ArrayList;

import br.com.kelso.voz.CadastrarActivity;
import br.com.kelso.voz.R;
import br.com.kelso.voz.dao.UserDao;
import br.com.kelso.voz.exception.UserEmailErradoException;
import br.com.kelso.voz.exception.UserEmailExistenteException;
import br.com.kelso.voz.exception.UserSenhaErradaException;
import br.com.kelso.voz.exception.UserVazioException;
import br.com.kelso.voz.modelo.User;

/**
 * Created by kelso on 06/10/2017.
 */

public class CadastroHelper {

    private EditText campoFristName;
    private EditText campoLastName;
    public  EditText campoEmail;
    public  EditText campoConfEmail;
    private EditText campoSenha;
    public  EditText campoConfSenha;
    private User usuario;
    private UserDao dao;
    private ArrayList<EditText> campos = new ArrayList<EditText>();


    public CadastroHelper(CadastrarActivity activity){
        campos.add(campoFristName = (EditText) activity.findViewById(R.id.cadastrar_frist_name));
        campos.add(campoLastName = (EditText) activity.findViewById(R.id.cadastrar_last_name));
        campos.add(campoEmail = (EditText) activity.findViewById(R.id.cadastrar_email));
        campos.add(campoConfEmail = (EditText) activity.findViewById(R.id.cadastrar_confEmail));
        campos.add(campoSenha = (EditText) activity.findViewById(R.id.cadastrar_senha));
        campos.add(campoConfSenha = (EditText) activity.findViewById(R.id.cadastrar_confSenha));
        usuario = new User();
        dao = new UserDao(activity);
    }

    private boolean camposVazio(){
        for (EditText campo: campos) {
            String str = campo.getText().toString();
            if (TextUtils.isEmpty(str)) {
                return true;
            }
        }
        return false;
    }

    private boolean emailCorreto(){
        String email = campoEmail.getText().toString();
        String confEmail = campoConfEmail.getText().toString();
        if(email.equals(confEmail)){
            return true;
        }
        return false;
    }
    private boolean senhaCorreta() {
        String senha = campoSenha.getText().toString();
        String confSenha = campoConfSenha.getText().toString();
        if(senha.equals(confSenha)){
            return true;
        }
        return false;
    }



   public User pegaCadastro() throws UserVazioException, UserEmailErradoException, UserSenhaErradaException, UserEmailExistenteException {

        if((!camposVazio()) && (emailCorreto()) && (senhaCorreta()) && (!dao.existeEmail(campoEmail.getText().toString()))){
            usuario.setFrist_name(campoFristName.getText().toString());
            usuario.setLast_name(campoLastName.getText().toString());
            usuario.setEmail(campoEmail.getText().toString());
            usuario.setSenha(campoSenha.getText().toString());
            return usuario;

        }else if (camposVazio()){

            throw new UserVazioException();

        }else if(!emailCorreto()){

            throw  new UserEmailErradoException();
        }else if(!senhaCorreta()){

            throw new UserSenhaErradaException();
        }else if (dao.existeEmail(campoEmail.getText().toString())){
            campoEmail.setError("Email já Cadastrado!!");
            campoConfEmail.setText("");
            campoSenha.setText("");
            campoConfSenha.setText("");
            throw new UserEmailExistenteException();

        }
        return null;
    }



}
