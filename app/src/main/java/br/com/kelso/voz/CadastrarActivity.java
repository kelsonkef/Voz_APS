package br.com.kelso.voz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import br.com.kelso.voz.dao.UserDao;
import br.com.kelso.voz.exception.UserEmailErradoException;
import br.com.kelso.voz.exception.UserEmailExistenteException;
import br.com.kelso.voz.exception.UserSenhaErradaException;
import br.com.kelso.voz.exception.UserVazioException;
import br.com.kelso.voz.fachada.CadastroHelper;
import br.com.kelso.voz.modelo.User;

public class CadastrarActivity extends AppCompatActivity {


    private CadastroHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);
        helper = new CadastroHelper(this);


        Button botaoEnviar = (Button) findViewById(R.id.cadastrar_enviar);
        botaoEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(helper.pegaCadastro() != null){
                        User usuario = helper.pegaCadastro();
                        UserDao dao = new UserDao(CadastrarActivity.this);
                        dao.insere(usuario);
                        Toast.makeText(CadastrarActivity.this, "Usuario "+ usuario.getFrist_name() + " Cadastrado", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (UserVazioException e) {
                    e.printStackTrace();
                    Toast.makeText(CadastrarActivity.this, "Por favor preenchar os Campos Vazios", Toast.LENGTH_SHORT).show();
                } catch (UserEmailErradoException e) {
                    e.printStackTrace();
                    helper.campoConfEmail.setError("Campos de Email não se coincidem");
                    helper.campoConfEmail.setText("");

                } catch (UserSenhaErradaException e) {
                    e.printStackTrace();
                    helper.campoConfSenha.setError("Campos de Senha não se coincidem");
                    helper.campoConfSenha.setText("");
                } catch (UserEmailExistenteException e) {
                    e.printStackTrace();

                }

            }
        });

    }

}
