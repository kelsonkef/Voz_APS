package br.com.kelso.voz.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.kelso.voz.modelo.Reclamacao;

/**
 * Created by kelso on 19/10/2017.
 */

public class ReclamacaoDao extends SQLiteOpenHelper {

    public ReclamacaoDao(Context context) {
        super(context, "Voz2", null, 1);
    }


    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Reclamacao(id INTEGER PRIMARY KEY , endereco TEXT NOT NULL, categoria TEXT, descricao TEXT, caminhoFoto TEXT);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Reclamacao";
        db.execSQL(sql);
        onCreate(db);
    }


    public void insere(Reclamacao reclamacao) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = pegaDadosReclamacao(reclamacao);
        db.insert("Reclamacao", null, dados);

    }

    @NonNull
    private ContentValues pegaDadosReclamacao(Reclamacao reclamacao) {
        ContentValues dados = new ContentValues();
        dados.put("endereco", reclamacao.getEndereco());
        dados.put("categoria", reclamacao.getCategoria());
        dados.put("descricao", reclamacao.getDescricao());
        dados.put("caminhoFoto", reclamacao.getCaminhoFoto());
        return dados;
    }

    public void deleta(Reclamacao reclamacao) {
        SQLiteDatabase db = getWritableDatabase();

        String[] params = {reclamacao.getId().toString()};
        db.delete("Reclamacao", "id = ?", params);
    }

    public List<Reclamacao> buscaReclamacoes()  {
        String sql = "SELECT * FROM Reclamacao;";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        List<Reclamacao> reclamacoes = new ArrayList<Reclamacao>();
        while (c.moveToNext()) {
            Reclamacao reclamacao = new Reclamacao();
            reclamacao.setId(c.getLong(c.getColumnIndex("id")));
            reclamacao.setEndereco(c.getString(c.getColumnIndex("endereco")));
            reclamacao.setCategoria(c.getString(c.getColumnIndex("categoria")));
            reclamacao.setDescricao(c.getString(c.getColumnIndex("descricao")));
            reclamacao.setCaminhoFoto(c.getString(c.getColumnIndex("caminhoFoto")));
            reclamacoes.add(reclamacao);

        }
        c.close();

        return reclamacoes;

    }
}
