package br.com.kelso.voz.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.kelso.voz.modelo.User;

/**
 * Created by kelso on 07/10/2017.
 */

public class UserDao extends SQLiteOpenHelper {


    public UserDao(Context context) {
        super(context, "Voz", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String slq = "CREATE TABLE Usuarios (id INTEGER PRIMARY KEY, frist_name VARCHAR(20) NOT NULL, last_name VARCHAR(20) NOT NULL, email VARCHAR(30) NOT NULL, senha VARCHAR(50) NOT NULL);";
        db.execSQL(slq);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Usuarios";
        db.execSQL(sql);
        onCreate(db);
    }


    public void insere(User usuario) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = pegaDadosUsuario(usuario);
        db.insert("Usuarios", null, dados);

    }

    private ContentValues pegaDadosUsuario(User usuario) {
        ContentValues dados = new ContentValues();
        dados.put("frist_name", usuario.getFrist_name());
        dados.put("last_name", usuario.getLast_name());
        dados.put("email", usuario.getEmail());
        dados.put("senha", usuario.getSenha());
        return dados;
    }


    public List<User> buscaUsuarios() {
        String sql = "SELECT * FROM Usuarios;";
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery(sql, null);
        List<User> usuarios = new ArrayList<User>();
        while (c.moveToNext()){
            User usuario = preencherUser(c);
            usuarios.add(usuario);
        }
        c.close();
        return usuarios;
    }

    @NonNull
    private User preencherUser(Cursor c) {
        User usuario = new User();
        usuario.setId(c.getLong(c.getColumnIndex("id")));
        usuario.setFrist_name(c.getString(c.getColumnIndex("frist_name")));
        usuario.setLast_name(c.getString(c.getColumnIndex("last_name")));
        usuario.setEmail(c.getString(c.getColumnIndex("email")));
        usuario.setSenha(c.getString(c.getColumnIndex("senha")));
        return usuario;
    }

    public boolean existeEmail(String email){
        SQLiteDatabase db = getReadableDatabase();
        String[] params={email.toString()};
        Cursor c = db.rawQuery("SELECT * FROM Usuarios WHERE email= ?", params);
        if(c.getCount() <=0){
            return false;
        }
        return true;
    }

    public User retornaUserEmail(String email){
        if(existeEmail(email)){
            SQLiteDatabase db = getReadableDatabase();
            String[] params={email.toString()};
            Cursor c = db.rawQuery("SELECT * FROM Usuarios WHERE email= ?", params);
            if(c.moveToNext()){
                User usuario = preencherUser(c);
                return usuario;
            }
        }
        return null;
    }



}
